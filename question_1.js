function findGeometryLogic(arr){ 
    // you can only write your code here!
    let result = null;
    for (let i=0; i < arr.length-1; i++) {
      if (arr[i+1]/arr[i] === 2 || arr[i+1]/arr[i] === 3) {
        result = true;
      }
      else {
        result = false;
      }
    } return result;
  }
  // TEST CASES
  console.log(
  
  findGeometryLogic  ([1, 3, 9, 27, 81])); // true
  console.log(
  
  findGeometryLogic  ([2, 4, 8, 16, 32])); // true
  console.log(
  
  findGeometryLogic  ([2, 4, 6, 8])); // false
  console.log(
  
  findGeometryLogic  ([2, 6, 18, 54])); // true
    
  console.log(findGeometryLogic  ([1, 2, 3, 4, 7, 9])); // false