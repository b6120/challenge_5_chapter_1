// Diberikan sebuah function 

// findClosestTarget  (arr) yang menerima satu parameter berupa array yang terdiri dari karakter. Function akan me-return jarak spasi antar karakter 'o' dengan karakter 'x' yang terdekat. Contoh, jika arr adalah ['x', ' ', 'o', ' ', ' ', 'x'], maka jarak terdekat dari 'o' ke 'x' adalah 2. Jika tidak ditemukan 'x' sama sekali, function akan me-return nilai 0.


function findClosestTarget(arr) {

// you can only write your code here! 
} 
 
// TEST CASES 
console.log(

findClosestTarget  ([' ', ' ', 'o', ' ', ' ', 'x', ' ', 'x'])); // 3

console.log(

findClosestTarget  (['o', ' ', ' ', ' ', 'x', 'x', 'x'])); // 4 
console.log(

findClosestTarget  (['x', ' ', ' ', ' ', 'x', 'x', 'o', ' '])); // 1 
console.log(

findClosestTarget  ([' ', ' ', 'o', ' '])); // 0 


function findClosestTarget(arr) {
    // you can only write your code here!
  
    // let xIndex = arr.indexOf('x');
    // let oIndex = arr.indexOf('o');
    // let result = Math.abs(xIndex - oIndex);
    // console.log(result);
  
    // Membuat list array indexOf dari letter 'x'
    const X = [];
    let idx = arr.indexOf("x");
    while (idx != -1) {
      X.push(idx);
      idx = arr.indexOf("x", idx + 1);
    }
  
    // Membuat list array indexOf dari letter 'o'
    const O = [];
    let ido = arr.indexOf("o");
    while (ido != -1) {
      O.push(ido);
      ido = arr.indexOf("o", ido + 1);
    }
  
    //   Membuat function untuk mencari smallest difference dari 2 array
    function smallestDifference(X, O, m, n) {
      let x = 0;
      let o = 0;
  
      // Initialize result as max value
      let result = Number.MAX_SAFE_INTEGER;
  
      // Jika x tidak ada di array yang menyebabkan index[0] dari undefined, maka berikan nilai 0
      if (X[0] == undefined) return 0;
      // Melakukan scan array x dan o, berdasarkan besar index arraynya
      else while (x < m && o < n) {
        if (Math.abs(X[x] - O[o]) < result) result = Math.abs(X[x] - O[o]);
        // Move Smaller Value
        if (X[x] < O[o]) x++;
        else o++;
      }
  
      // Return final result
      return result;
    }
  
    // Calculate size of Both arrays
    let m = X.length;
    let n = O.length;
  
    // console log smalles Difference
    return smallestDifference(X, O, m, n);
  }
  
  // TEST CASES
  console.log(findClosestTarget([" ", " ", "o", " ", " ", "x", " ", "x"])); // 3
  console.log(findClosestTarget(["o", " ", " ", " ", "x", "x", "x"])); // 4
  console.log(findClosestTarget(["x", " ", " ", " ", "x", "x", "o", " "])); // 1
  console.log(findClosestTarget([" ", " ", "o", " "])); // 0
  console.log(findClosestTarget([" ", "o", " ", "x", "x", " ", " ", "x"])); // 2
  
