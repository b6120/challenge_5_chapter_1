// Kelompokan animal berdasarkan hurup depannya. Jika C maka 1 array isinyya yang depannya huruf C. Jika A  maka harus kumpulkan A dalam satu array.

function groupingAnimalBasedOnHurufDepan(animals) {
    // you can only write your code here!
    let result = [];
    animals.sort();
   for(let i = 0; i < animals.length ; i++) {
       const item = animals[i];
       const firstLetter = item[0]
       if(!result[animals[i][0]]) {
           result[firstLetter] = [];
       }

       if(result[firstLetter].indexOf(item) < 0) {
           result[firstLetter].push(item);
       }
   }
   return result;
}
    // TEST CASES
    
    console.log(
    
    groupingAnimalBasedOnHurufDepan  (['cacing', 'ayam', 'kuda', 'anoa', 'kancil']));
    // [ ['ayam', 'anoa'], ['cacing'], ['kuda', 'kancil'] ]
    console.log(
    
    groupingAnimalBasedOnHurufDepan  (['cacing', 'ayam', 'kuda', 'anoa', 'kancil', 'unta', 'cicak' ]));
    // [ ['ayam', 'anoa'], ['cacing', 'cicak'], ['kuda', 'kancil'], ['unta'] ]


//     function groupingAnimalBasedOnHurufDepan(animals) {
//         // you can only write your code here!
//         animals.sort((a, b) => a.localeCompare(b));
      
//         let data = animals.reduce((r, e) => {
//           // Ambil huruf pertama dari tiap nama animal
//           let group = e[0];
//           // Membuat object baru, apabila tidak ada objet tersebut sebelumnya. Dalam hal ini object yang nantinya berisi nama binatang berdasarkan alphabet.
//           if (!r[group]) r[group] = [e];
//           // ketika sudah ada object array diatas, maka push array binatang ke object tersebut.
//           else r[group].push(e);
//           // return accumulator
//           return r;
//         }, {});
//           // mendapatkan value dari arrays menggunakan method object values
//   let result = Object.values(data);

//   return result;
// }